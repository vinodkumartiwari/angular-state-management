import { Router } from '@angular/router';
import { of } from 'rxjs';

import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';

describe('AuthGuardService', () => {
  let guard: AuthGuardService;
  let store: any;
  let router: any;

  beforeEach(() => {
    store = jasmine.createSpyObj(['select']);
    router = jasmine.createSpyObj(['navigate']);
    guard = new AuthGuardService(store, router);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  describe('canActivate', () => {
    it('should be called', () => {
      store.select.and.returnValue(of(true));
      spyOn(guard, 'canActivate');
      let canActivateResult = guard.canActivate();
      expect(guard.canActivate).toHaveBeenCalled();
    });

    it('should call navigate when not authorized', () => {
      store.select.and.returnValue(of(false));
        expect(router.navigate);
    });

    // it('should call navigate when authorized', () => {
    //   store.select.and.returnValue(of(true));

      
    //   guard.canActivate().subscribe((data) => {
    //     expect(router.navigate).toHaveBeenCalledTimes(0);
    //   });
    // });
  });
});