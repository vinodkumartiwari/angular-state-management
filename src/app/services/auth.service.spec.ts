import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let httpClient;

  let mockAuthService = {
    logIn: () => {}
  }
  
  TestBed.configureTestingModule({
    providers: [
      { provide: AuthService, useValue: mockAuthService }
    ]
  })
  
  it('should authenticate', () => {
    spyOn(mockAuthService, 'logIn').and.returnValue();
  })

  beforeEach(() => {
    httpClient = jasmine.createSpyObj(['post']);

    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: httpClient,
        },
      ],
    });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // describe('should authenticate username and password', () => {
  //   let username: 'MTN_user@mtn.com';
  //   let password: 'MTN281#^@*';
  //   it('should return success when credentails are valid', () => {
  //     service.logIn(username, password).subscribe((res) => {
  //       expect(res.status).toBe('{ "id": 1, "username": "MTN_user@test.com", "password": "MTN281#^@*", "firstName": "MTN", "lastName": "User" }');
  //     });
  //   });

  //   it('should throw error when credentails are valid', () => {
  //     service.logIn(username, password).subscribe(
  //       (data) => {},
  //       (err) => {
  //         expect(err).toBe('Username or password is incorrect');
  //       }
  //     );
  //   });
  // });
});