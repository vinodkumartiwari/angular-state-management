import { Component, OnInit } from "@angular/core";
import { Auth } from "../../models/auth";
import { Store } from "@ngrx/store";
import { AuthState } from "../store/reducer/auth.reducer";
import { Login } from "../store/action/auth.actions";
import { Observable } from "rxjs";
import { userLoginState } from "../store/selector/auth.selectors";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  userLoginState$: Observable<boolean>;
  loginForm: FormGroup = this.formBuilder.group({
    username: ['', []],
    password: ['',[],
    ],
  });
  submitted = false;

  constructor(private store: Store<AuthState>, private router: Router, private formBuilder: FormBuilder) 
  {
    this.userLoginState$ = this.store.select(userLoginState);
  }

  Login(username: string, password: string): void {
    this.submitted = true;
  // stop here if form is invalid
  
    const auth = new Auth();
    auth.username = username;
    auth.password = password;
    this.store.dispatch(Login({ auth: auth }));
    this.userLoginState$.subscribe(loginState => {
      if(loginState){
        this.router.navigateByUrl('dashboard');
      } else {
        console.log('Failed Login! Invalid Credentials! :-)\n\n' + JSON.stringify(this.loginForm.value));
      }
    })
}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]]
    });
}
//   onSubmit() {
//     this.submitted = true;

//     // stop here if form is invalid
//     if (this.loginForm.invalid) {
//         return;
//     }

//   alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))
// }
get f() { return this.loginForm.controls; }

}
