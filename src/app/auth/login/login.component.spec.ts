import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { UserState } from '../store/reducer/auth.reducer';
import { ReplaySubject } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let store: MockStore;
  let router;
  const initialState: UserState = {
    loggedIn: false,
    error: '',
  };
  let errMessage$ = new ReplaySubject(1);
  let isUserloggedin$ = new ReplaySubject(1);
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule],
      providers: [provideMockStore({ initialState })],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    router = TestBed.inject(Router);
    spyOn(store, 'select')
      .and.returnValue(isUserloggedin$.asObservable())
      .and.returnValue(errMessage$.asObservable());
    fixture.detectChanges();
  });

 it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assign userLoggedIn$ on login', async () => {
    expect(component.userLoginState$).toBeDefined();
  });

  describe('Login', () => {
    it('should call Login', () => {
      let componentLogin = spyOn(component, 'Login');
      spyOn(store, 'dispatch').and.returnValue(undefined);
      component.Login('MTN_user@test.com','MTN281#^@*');
      expect(componentLogin).toHaveBeenCalled();
    });
  });
  describe('Login', () => {
    it('should dispatch action', () => {
      let storeDispatch = spyOn(store, 'dispatch').and.returnValue(undefined);
      // spyOn(component, 'checkAndNavigate');
      component.Login('MTN_user@test.com','MTN281#^@*');

      expect(storeDispatch).toHaveBeenCalled();
    });
  });

});
