import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {Auth} from '../../models/auth';
import {select, Store} from '@ngrx/store';
import {selectAuth} from '../store/selector/auth.selectors';
import {AuthState} from '../store/reducer/auth.reducer';
import { Router } from '@angular/router';
import { Logout } from '../store/action/auth.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  auth$: Observable<Auth[]>;
  constructor(private store: Store<AuthState>, private router: Router) {
    this.auth$ = this.store.pipe(select(selectAuth));
  }

  logout() {
    this.store.dispatch(Logout());
    this.router.navigate(['/login']);
  }
}
