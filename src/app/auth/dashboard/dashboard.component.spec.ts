import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { UserState } from '../store/reducer/auth.reducer';

import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
//  let fixture: ComponentFixture<FirstPageComponent>;
  let mockRouter;
  let mockStore;
  let router: Router;
  let store: MockStore;
  const initialState: UserState = {
    loggedIn: false,
    error: '',
  };
  beforeEach(() => {
    mockRouter = jasmine.createSpyObj(['navigate']);
    mockStore = jasmine.createSpyObj(['dispatch', 'select']);
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [RouterTestingModule.withRoutes([])],
      providers: [provideMockStore({ initialState })],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should route back to login after logout', () => {
    it('should navigate to login when user logged out', () => {
      spyOn(store, 'select').and.returnValue(of(false));
        expect(router.navigate);
    });
  });

  describe('logout action', () => {
    it('should dispatch Logout', () => {
      spyOn(store, 'dispatch').and.returnValue(undefined);
      spyOn(store, 'select').and.returnValue(of(false));
      spyOn(router, 'navigate');
      component.logout();
      expect(store.dispatch).toHaveBeenCalled();
    });

    it('should call logout method', () => {
      spyOn(store, 'dispatch').and.returnValue(undefined);
      spyOn(component, 'logout').and.returnValue(undefined);
      spyOn(router, 'navigate');
      component.logout();
      expect(component.logout).toHaveBeenCalled();
    });
  });
  
});
