import { createAction, props } from '@ngrx/store';
import {Auth} from '../../../models/auth';

export const Login = createAction(
  '[Auth] Validate Login',
  props<{auth: Auth}>()
);

export const LoginOk = createAction(
  '[Auth] Login Succeeded',
  props<{auth: Auth}>()
);

export const LoginError = createAction(
  '[Auth] Login Failed'
);

export const Logout = createAction(
  '[Auth] Logout'
  );