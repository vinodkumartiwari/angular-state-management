import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { AuthService } from "src/app/services/auth.service";
import { switchMap, map, catchError } from "rxjs/operators";
import { Login, LoginError, LoginOk } from "../action/auth.actions";
import { of } from "rxjs/internal/observable/of";

@Injectable()
export class AuthEffects {
  constructor(private action$: Actions, private auth: AuthService) {}

  Login$ = createEffect(() =>
    this.action$.pipe(
      ofType(Login),
      switchMap((action) => {
        return this.auth.logIn(action.auth.username, action.auth.password).pipe(
          map((data) => LoginOk({auth: action.auth})),
          catchError((error) => {
            return of(LoginError());
          })
        );
      })
    )
  );
}
