import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of, ReplaySubject, throwError } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
import { AuthEffects } from '../effect/auth.effects';
import { UserState } from '../reducer/auth.reducer';
import { Login, LoginError, LoginOk } from '../action/auth.actions';
import { take } from 'rxjs/operators';

class MockAuthService {
    login(username: string, password: string) {
        of({ "id": 1, "username": "MTN_user@test.com", "password": "MTN281#^@*", "firstName": "MTN", "lastName": "User" })
    }
}
describe('User Effects', () => {
    let actions$: any;
    let effects: AuthEffects;
    let store: MockStore<UserState>;
    let authService: AuthService;
    const initialState: UserState = {
        loggedIn: false,
        error: '',
    };
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthEffects,
                provideMockActions(() => actions$),
                provideMockStore({ initialState }),
                { provide: AuthService, useClass: MockAuthService },
            ],
        });
        effects = TestBed.inject(AuthEffects);
        store = TestBed.inject(MockStore);
        authService = TestBed.inject(AuthService);
    });

    // it('should be created', () => {
    //     expect(effects).toBeTruthy();
    // });

    it('should result in LOGIN SUCCESS when API succeeds', () => {
        actions$ = new ReplaySubject(1); 
        actions$.next({
            type: '[Auth] Validate Login',
            auth: {
                username: '',
                password: '',
            },
        });
        effects.Login$.pipe(take(1)).subscribe((res) => {
            expect(res.type).toBe('[Auth] Login Succeeded');
        });
    });

    // it('should result in LOGIN FAIL when API errors', () => {
    //   spyOn(authService, 'logIn').and.returnValue(throwError('error'));
    //   actions$ = new ReplaySubject(1);
    //   actions$.next({
    //     type: LoginOk,
    //     userCred: {
    //       username: '',
    //       password: '',
    //     },
    //   });

    //   effects.Login$.pipe(take(1)).subscribe((res) => {
    //     expect(res.type).toBe(LoginError.toString());
    //   });
    // });
});