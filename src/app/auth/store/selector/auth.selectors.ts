import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromAuth from '../reducer/auth.reducer';

export const selectAuthState = createFeatureSelector<fromAuth.AuthState>(
  fromAuth.authFeatureKey,
);

export const selectAuth = createSelector(
  selectAuthState,
  (state: fromAuth.AuthState) => state.auth
);

export const userLoginState = createSelector(
  selectAuthState,
  (state: fromAuth.AuthState) => state.auth.length > 0
);
