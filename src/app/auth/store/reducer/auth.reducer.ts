import { Action, createReducer, on } from "@ngrx/store";
import * as AuthActions from "../action/auth.actions";
import { Auth } from "../../../models/auth";


export const authFeatureKey = "auth";
export interface AuthState {
  auth: Auth[];
  loggedIn:boolean;
  error: string;
}

export interface UserState {
  loggedIn: boolean;
  error: string;
}

export const initialState: AuthState = {
  auth: [],
  loggedIn: false,
  error: '',
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.LoginOk, (state: AuthState, { auth }) => ({
    ...state,
    auth: [...state.auth, auth],
    loggedIn: true,
    error: '',
  })),
  on(AuthActions.LoginError, (state: AuthState) => ({
    auth: [],
    loggedIn: false,
    error: '[Auth] Login Failed',
  }))
);

export function reducer(state: AuthState | undefined, action: Action): any {
  return authReducer(state, action);
}
