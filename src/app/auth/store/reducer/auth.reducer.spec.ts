import { LoginError, LoginOk, Logout } from '../action/auth.actions';
import { initialState, authReducer, AuthState } from '../reducer/auth.reducer';

describe('Auth Reducer', () => {
  describe('LoginOk action', () => {
    let mockInitialState: AuthState;
    beforeEach(() => {
  
    });

    it('should update the loggedIn true', () => {
      const state = authReducer(mockInitialState, LoginOk);
      expect(state.loggedIn).toBe(true);
    });

    it('should clear the error value', () => {
        const state = authReducer(mockInitialState, LoginOk);
        expect(state.error).toBe('');
      });
  });

  describe('LoginFail action', () => {
    let mockInitialState: AuthState;
    beforeEach(() => {
      mockInitialState = {
        auth: [],
        loggedIn: true,
        error: '',
      };
    });

    it('should update the loggedIn false', () => {
      const state = authReducer(mockInitialState, LoginError);
      expect(state.loggedIn).toBe(false);
    });
    
    it('should update the error value', () => {
        const state = authReducer(mockInitialState, LoginError());
        expect(state.error).toBe('[Auth] Login Failed');
      });
  });

  describe('LogOut action', () => {
    let mockInitialState: AuthState;
    beforeEach(() => {
      mockInitialState = {
        auth:[],
        loggedIn: true,
        error: '',
      };
    });

    // it('should restore initial state', () => {
    //   const state = authReducer(mockInitialState, Logout);
    //   expect(state).toEqual(initialState);
    // });
  });
});