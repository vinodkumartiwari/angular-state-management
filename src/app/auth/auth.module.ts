import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {authFeatureKey, reducer} from './store/reducer/auth.reducer';
import {StoreModule} from '@ngrx/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [DashboardComponent, LoginComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    StoreModule.forFeature(authFeatureKey, reducer),
  ],
  exports: [
    DashboardComponent,
    LoginComponent
  ]
})
export class AuthModule { }
